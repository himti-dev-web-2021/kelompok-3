<?php

namespace Database\Seeders;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'=>'Fazriansyah',
            'username'=>'admin',
            'password'=>bcrypt('admin123'),
            'email'=>'nerwinhd801@gmail.com',
        ]);
    }
}
