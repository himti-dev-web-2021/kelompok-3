<?php

namespace Database\Seeders;

use App\Models\Genre;
use Illuminate\Database\Seeder;
use Ramsey\Uuid\Uuid;

class GenreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $genres = collect([
            'Alternative',
            'Anime',
            'Blues',
            'Children',
            'Classical',
            'Comedy',
            'Country',
            'Dance',
            'Dangdut',
            'Disney',
            'Easy Listening',
            'Elektronic',
            'Enka',
            'French Pop',
            'German Folk',
            'German Pop',
            'Fitness & Workout',
            'Hip-Hop/Rap',
            'Holiday',
            'Indie Pop',
            'Industrial',
            'Inspirational – Christian & Gospel',
            'Instrumental',
            'J-Pop',
            'Jazz',
            'K-Pop',
            'Karaoke',
            'Kayokyoku',
            'Latin',
            'Metal',
            'New Age',
            'Opera',
            'Pop',
            'R&B/Soul',
            'Reggae',
            'Rock',
            'Singer/Songwriter',
            'Soundtrack',
            'Vocal',
            'World',
        ]);

        $genres->each(function($genre) {
            Genre::create([
                'name'=>$genre,
                'uuid'=>Uuid::uuid4()->toString()
            ]);
        });
    }
}
