@extends('layouts.back')

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header text-center"><h2>Explore</h2></div>
                        <div class="card-body">
                            <div class="container-fluid">
                                <div class="row justify-content-center">
                                    <div class="col-4">
                                        <h4 align="center">Genre</h4>
                                        <hr>
                                        @foreach ($genre as $v)
                                            <a href="{{route('explore.showgenre', $v->name)}}" class="text-decoration-none text-dark">
                                                <p class="py-1">{{$v->name}}</p>
                                            </a>
                                        @endforeach
                                    </div>
                                    <div class="col-8">
                                        <h4 align="center">Music</h4>
                                        <hr>
                                        @foreach ($music as $v)
                                            <div class="card mb-2 mr-3 border-0 d-inline-block py-2">
                                                @if ($v->artist->foto)
                                                    <a href="{{route('explore.showartist', $v->artist->name)}}">
                                                        <img class="img-fluid" style="width: 275px; height: 180px; object-fit: cover; object-position: center;"
                                                        src="{{ $v->artist->takeFoto }}" class="card-img-top" alt="{{$v->artist->name}}">
                                                    </a>
                                                @endif
                                                <div class="card-body text-center">        
                                                    <a href="{{route('explore.showartist', $v->artist->name)}}" class="text-decoration-none text-dark">
                                                        <strong>{{$v->artist->name}}</strong> -
                                                    </a>
                                                    <a href="{{route('explore.showmusic', $v->uuid)}}" class="text-decoration-none text-dark">
                                                        <small>{{$v->title}}</small>
                                                    </a>
                                                </div>
                                            </div>
                                        @endforeach                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection