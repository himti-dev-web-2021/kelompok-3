@extends('layouts.back')

@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-outline card-primary">
                    <div class="card-header">
                        <ol class="breadcrumb float-sm-right bg-transparent">
                            <li class="breadcrumb-item"><a href="#">Artist</a></li>
                            <li class="breadcrumb-item active">Index</li>
                        </ol>
                        <h3 class="card-title">
                            <a href="{{ route('artist.tambah') }}" class="btn btn-primary" style="background-color: #143ebd; color: white;">
                                <i class="fas fa-plus-circle"></i> Add Artist
                            </a>
                        </h3>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-sm">
                                <thead>
                                    <tr>
                                        <th style="background-color: #143ebd; color:white;" class="text-center">Foto</th>
                                        <th style="background-color: #143ebd; color:white;" class="text-center">Name</th>
                                        <th style="background-color: #143ebd; color:white;" class="text-center">Genre</th>
                                        <th style="background-color: #143ebd; color:white;" class="text-center">Biografi</th>
                                        <th style="background-color: #143ebd; color:white;" class="text-center" colspan="2">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($artist as $v)
                                        <tr class="">
                                            <td>
                                                @if ($v->foto)
                                                    <img src="{{ $v->takeFoto }}" class="d-block" style="margin: 0 auto; width:150px; height:200px;" alt="{{$v->name}}">
                                                @endif
                                            </td>
                                            <td>{{$v->name}}</td>
                                            <td>{{$v->genre->name}}</td>
                                            <td>{!! Str::limit(nl2br($v->bio), 100, '...') !!}</td>
                                            <td class="text-center"><a class="btn btn-primary btn-sm" href="{{route('artist.edit', $v->uuid)}}">Edit</a></td>
                                            <td class="text-center">
                                                <form action="{{route('artist.delete', $v)}}" method="post" class="form">
                                                    @csrf
                                                    @method('delete')
                                                    <button onclick="return confirm('Anda yakin ingin menghapusnya?');" class="btn btn-danger btn-sm" type="submit">Delete</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="card-footer clearfix pagination-footer">
                            {{$artist->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection