@extends('layouts.back')

@section('content')
    <section class="content">
        <div class="container-fluid">
            @if (Auth::user()->username != 'admin')
            <div class="row justify-content-md-center">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header text-center"><h3>Profile</h3></div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <p>
                                        Username: <strong>{{ Auth::user()->username }}</strong><br>
                                        Name: {{ Auth::user()->name }} <br>
                                        Email: {{ Auth::user()->email }}
                                    </p>
                                </div>
                                <div class="col-md-8">
                                    <div class="table-responsive">
                                        <h4 align="center">Your Request</h4>
                                        <table class="table table-bordered table-hover table-sm">
                                            <thead>
                                                <tr>
                                                    <th style="background-color: #143ebd; color:white;" class="text-center">Artist</th>
                                                    <th style="background-color: #143ebd; color:white;" class="text-center">Title</th>
                                                    <th style="background-color: #143ebd; color:white;" class="text-center" colspan="2">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($permintaan as $v)
                                                    <tr class="text-center">
                                                        <td>{{$v->artist}}</td>
                                                        <td>{{$v->title}}</td>
                                                        <td><a class="btn btn-primary btn-sm" href="{{route('permintaan.edit', $v->uuid)}}">Edit</a></td>
                                                        <td>
                                                            <form action="{{route('permintaan.delete', $v)}}" method="post" class="form">
                                                                @csrf
                                                                @method('delete')
                                                                <button onclick="return confirm('Anda yakin ingin menghapusnya?');" class="btn btn-danger btn-sm" type="submit">Delete</button>
                                                            </form>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @else
            <div class="row justify-content-md-center">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header text-center"><h3>Profile</h3></div>
                        <div class="card-body">
                            <p>
                                Username: <strong>{{ Auth::user()->username }}</strong><br>
                                Name: {{ Auth::user()->name }} <br>
                                Email: {{ Auth::user()->email }}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </section>
@endsection