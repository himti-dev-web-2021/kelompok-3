@extends('layouts.back')

@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-outline card-primary">
                    <div class="card-header">
                        <ol class="breadcrumb float-sm-right bg-transparent">
                            <li class="breadcrumb-item"><a href="#">Music</a></li>
                            <li class="breadcrumb-item active">Index</li>
                        </ol>
                        <h3 class="card-title">
                            <a href="{{ route('music.tambah') }}" class="btn btn-primary" style="background-color: #143ebd; color: white;">
                                <i class="fas fa-plus-circle"></i> Add Music
                            </a>
                        </h3>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-sm">
                                <thead>
                                    <tr>
                                        <th style="background-color: #143ebd; color:white;" class="text-center">Artist</th>
                                        <th style="background-color: #143ebd; color:white;" class="text-center">Title</th>
                                        <th style="background-color: #143ebd; color:white;" class="text-center">Writer</th>
                                        <th style="background-color: #143ebd; color:white;" class="text-center">Tahun Rilis</th>
                                        <th style="background-color: #143ebd; color:white;" class="text-center">Lyric</th>
                                        <th style="background-color: #143ebd; color:white;" class="text-center">Music</th>
                                        <th style="background-color: #143ebd; color:white;" class="text-center" colspan="2">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($music as $v)
                                        <tr class="">
                                            <td>{{$v->artist->name}}</td>
                                            <td>{{$v->title}}</td>
                                            <td>{{$v->writer}}</td>
                                            <td>{{$v->tgl}}</td>
                                            <td>{!! Str::limit(nl2br($v->lyric), 140, '...') !!}</td>
                                            <td>
                                                <iframe src="{{'https://open.spotify.com/embed/track/' . $v->url . '?theme=0'}}" width="100%" height="80" frameBorder="0" allowtransparency="true" allow="encrypted-media"></iframe>
                                            </td>
                                            <td class="text-center"><a class="btn btn-primary btn-sm" href="{{route('music.edit', $v->uuid)}}">Edit</a></td>
                                            <td class="text-center">
                                                <form action="{{route('music.delete', $v)}}" method="post" class="form">
                                                    @csrf
                                                    @method('delete')
                                                    <button onclick="return confirm('Anda yakin ingin menghapusnya?');" class="btn btn-danger btn-sm" type="submit">Delete</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="card-footer clearfix pagination-footer">
                            {{$music->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection