@extends('layouts.back')

@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-outline card-primary">
                    <div class="card-header">
                        Add Music
                    </div>
                    <div class="card-body">
                        <form action="{{ route('music.save') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group row">
                                <div class="col-md-2">
                                    <label class="col-form-label">Artist</label>
                                </div>
                                <div class="col-md-10">
                                    <select name="artist_id" id="artist_id" class="form-control @error('artist_id') is-invalid @enderror">
                                        <option value="" selected>Silahkan Pilih</option>
                                        @foreach($artist as $v)
                                            <option {{ (old('artist_id') == $v->id ) ? 'selected' : '' }} value="{{ $v->id }}">{{ $v->name }}</option>
                                        @endforeach
                                    </select>
                                    @error('artist_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-2">
                                    <label class="col-form-label">Title</label>
                                </div>
                                <div class="col-sm-10">
                                    <input type="text" name="title" id="title" class="form-control @error('title') is-invalid @enderror" value="{{ old('title') }}" maxlength="150">
                                    @error('title')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-2">
                                    <label class="col-form-label">Tahun Rilis</label>
                                </div>
                                <div class="col-md-10">
                                    <input type="text" name="tgl" id="tgl" class="form-control @error('tgl') is-invalid @enderror" value="{{ old('tgl') }}" maxlength="4">
                                    @error('tgl')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-2">
                                    <label class="col-form-label">Lyric</label>
                                </div>
                                <div class="col-md-10">
                                    <textarea type="text" name="lyric" id="lyric" class="form-control @error('lyric') is-invalid @enderror">{{ old('lyric') }}</textarea>
                                    @error('lyric')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-2">
                                    <label for="writer" class="col-form-label">Writer</label>
                                </div>
                                <div class="col-md-10">
                                    <input type="text" name="writer" id="writer" class="form-control @error('writer') is-invalid @enderror" value="{{ old('writer') }}">
                                    @error('writer')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-2">
                                    <label for="url" class="col-form-label">Music URL</label>
                                </div>
                                <div class="col-md-10">
                                    <input type="text" name="url" id="url" class="form-control @error('url') is-invalid @enderror" value="{{ old('url') }}" maxlength="150">
                                    @error('url')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <a href="{{ route('music') }}" class="btn btn-danger"><i class="fas fa-arrow-alt-circle-left"></i> Back</a>
                            <button type="submit" class="btn btn-primary float-right" style="background-color: #143ebd; color: white;"><i class="fas fa-save"></i> Save</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection