@extends('layouts.back')

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row justify-content-md-center">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title d-inline-block">
                                Explore: Artist
                            </h3>
                            <ol class="breadcrumb float-sm-right bg-transparent">
                                <li class="breadcrumb-item"><a href="{{route('explore.bygenre')}}">Genre</a></li>
                                <li class="breadcrumb-item active">Name</li>
                            </ol>
                        </div>
                        <div class="card-body">
                            @foreach ($artist as $v)
                                @include('explore.partials.index_card-body')
                            @endforeach
                        </div>
                        <div class="card-footer">
                            {{ $artist->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection