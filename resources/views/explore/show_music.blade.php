@extends('layouts.back')

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row justify-content-md-center">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title d-inline-block">
                                Explore: Music
                            </h3>
                        </div>
                        <div class="card-body">
                            <div class="d-flex-container">
                                <div class="row">
                                    <div class="col-12 text-center">
                                        <div class="card mb-4 border-0 d-inline-block">
                                            <div class="card-body text-center">
                                                <strong>
                                                    <a class="text-decoration-none text-dark" href="{{route('explore.showartist', $music->artist->name)}}">{{$music->artist->name}}</a> - {{$music->title}}
                                                </strong> <br>
                                                <i class="text-secondary">Writer - {{$music->writer}}</i>
                                            </div>
                                            <div class="card-footer">
                                                <iframe src="{{'https://open.spotify.com/embed/track/' . $music->url . '?theme=0'}}" width="100%" height="80" frameBorder="0" allowtransparency="true" allow="encrypted-media"></iframe>
                                            </div>
                                        </div>
                                        <hr>
                                        <h3>Lyric:</h3>
                                        <p>
                                            {!! nl2br($music->lyric) !!}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection