@extends('layouts.back')

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row justify-content-md-center">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title d-inline-block">
                                Explore: Artist
                            </h3>
                        </div>
                        <div class="card-body">
                            <div class="d-flex-container">
                                <div class="row">
                                    <div class="col-4 text-center">
                                        <div class="card mb-4 border-0 d-inline-block">
                                            @if ($artist->foto)
                                                <img class="img-fluid" style="width: 100%; object-fit: cover; object-position: center;"
                                                src="{{ $artist->takeFoto }}" class="card-img-top">
                                            @endif
                                            <div class="card-body text-center">
                                                <strong>{{$artist->name}}</strong> <br>
                                                <div>
                                                    Genre :
                                                    <a href="{{route('explore.showgenre', $artist->genre->name)}}" class="text-decoration-none text-secondary">
                                                        {{$artist->genre->name}}
                                                    </a>
                                                </div>
                                                <div>{{ 'Total Music: '. $artist->music->count() }}</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-8">
                                        <div class="title text-center">
                                            <h4>Biografi</h4>
                                        </div>
                                        <p align="justify">
                                              {!!nl2br($artist->bio)!!}
                                        </p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-hover table-sm">
                                            <thead>
                                                <tr>
                                                    <th style="background-color: #143ebd; color:white;" class="text-center">No</th>
                                                    <th style="background-color: #143ebd; color:white;" class="text-center">Title</th>
                                                    <th style="background-color: #143ebd; color:white;" class="text-center">Tahun Rilis</th>
                                                    <th style="background-color: #143ebd; color:white;" class="text-center">Play</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if ($music->count() != 0)
                                                    @foreach ($music as $v)
                                                    <tr class="text-center">
                                                        <td>{{ $music->count() * ($music->currentPage() - 1) + $loop->iteration }}</td>
                                                        <td>
                                                            <a href="{{route('explore.showmusic', $v->uuid)}}" class="text-decoration-none text-dark">{{$v->title}}</a>
                                                        </td>
                                                        <td>{{$v->tgl}}</td>
                                                        <td>
                                                            <iframe src="{{'https://open.spotify.com/embed/track/' . $v->url . '?theme=0'}}" width="100%" height="80" frameBorder="0" allowtransparency="true" allow="encrypted-media"></iframe>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                @else
                                                    <tr class="text-center">
                                                        <td colspan="4">The Music Still Not Available</td>
                                                    </tr>
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            {{$music->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection