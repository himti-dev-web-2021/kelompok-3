@extends('layouts.back')

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row justify-content-md-center">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title d-inline-block">
                                Search: {{$query}}
                            </h3>
                        </div>
                        <div class="card-body">
                            @forelse ($music as $v)
                                <div class="card mb-2 mr-3 border-0 d-inline-block">
                                    @if ($v->artist->foto)
                                        <a href="{{route('explore.showartist', $v->artist->name)}}">
                                            <img class="img-fluid" style="width: 275px; height: 180px; object-fit: cover; object-position: center;"
                                            src="{{ $v->artist->takeFoto }}" class="card-img-top" alt="{{$v->artist->name}}">
                                        </a>
                                    @endif
                                    <div class="card-body text-center">        
                                        <a href="{{route('explore.showartist', $v->artist->name)}}" class="text-decoration-none text-dark">
                                            <strong>{{$v->artist->name}}</strong> -
                                        </a>
                                        <a href="{{route('explore.showmusic', $v->uuid)}}" class="text-decoration-none text-dark">
                                            <small>{{$v->title}}</small>
                                        </a>
                                    </div>
                                </div>
                            @empty    
                                <div class="alet alert-info">
                                    There are no music with "{{$query}}" title.
                                </div>
                            @endforelse
                        </div>
                        <div class="card-footer">
                            {{ $music->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection