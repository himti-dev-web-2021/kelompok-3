<div class="card mb-2 mr-3 border-0 d-inline-block">
    @if ($v->foto)
        <a href="{{route('explore.showartist', $v->name)}}">
            <img class="img-fluid" style="width: 275px; height: 180px; object-fit: cover; object-position: center;"
            src="{{ $v->takeFoto }}" class="card-img-top" alt="{{$v->name}}">
        </a>
    @endif
    <div class="card-body text-center">
        <a href="{{route('explore.showartist', $v->name)}}" class="text-decoration-none text-dark">
            <strong>{{$v->name}}</strong> |
        </a>
        <a href="{{route('explore.showgenre', $v->genre->name)}}" class="text-decoration-none text-secondary">
            <small>{{$v->genre->name}}</small>
        </a>
    </div>
</div>