<li class="nav-item mr-2">
    <form action="{{ route('explore.searchmusic')}}" method="get" class="d-flex">
        <input name="query" class="form-control me-2" type="search" placeholder="Search Music" aria-label="Search">
        <button class="btn btn-sm btn-outline-primary" type="submit">Search</button>
    </form>
</li>
<li class="nav-item mr-2">
    <a href="{{ route('explore') }}" class="nav-link text-light">
        Artist
    </a>
</li>