<div class="footer">
    <footer class="bg-dark text-light py-4">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-4 m-auto py-2 text-center">
                    <img class="w-50" style="height: 120px;" src="{{asset('favicon/android-chrome-192x192.png')}}" alt="logo NZMusic">
                </div>
                <div class="col-4 py-2">
                    <h4>About</h4>
                    <p align="justify">
                        Find lyric and listen to the music for whenever you want.
                        Music resource that we used was taken from <a class="text-decoration-none" target="_blank" href="https://open.spotify.com/"><strong>Spotify</strong></a> as embed code.
                        Website builded by using: Laravel Framework, PHP, HTML, CSS, and PostgreSQL Database.
                    </p>
                </div>
                <div class="col-4 py-2">
                    <h4>Contact</h4>
                    <p align="left">
                        <strong>Group 3 of HIMTI Dev Web 2021</strong> <br>
                        Member list: <br>
                        1. Fazriansyah | <a href="https://gitlab.com/fazrian.syah19" target="_blank">@fazrian.syah19</a> <br>
                        2. Siti Nurliana | <a href="https://gitlab.com/sitinurlianaana" target="_blank">@sitinurlianaana</a> <br>
                        3. Andy Rachman | <a href="https://gitlab.com/Piuh" target="_blank">@Piuh</a> <br>
                    </p>
                </div>
            </div>
            <hr>
            <div class="text-center">
                <strong>Copyright &copy; 2021 NZMusic.</strong>
            </div>
        </div>
    </footer>
</div>
