@extends('layouts.back')

@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-outline card-primary">
                    <div class="card-header">
                        Add Request
                    </div>
                    <div class="card-body">
                        <form action="{{ route('permintaan.save') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group row">
                                <div class="col-sm-2">
                                    <label class="col-form-label">Artist</label>
                                </div>
                                <div class="col-sm-10">
                                    <input type="text" name="artist" id="artist" class="form-control @error('artist') is-invalid @enderror" value="{{ old('artist') }}" maxlength="150">
                                    @error('artist')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-2">
                                    <label class="col-form-label">Title Song</label>
                                </div>
                                <div class="col-sm-10">
                                    <input type="text" name="title" id="title" class="form-control @error('title') is-invalid @enderror" value="{{ old('title') }}" maxlength="150">
                                    @error('title')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary float-right" style="background-color: #143ebd; color: white;"><i class="fas fa-save"></i> Save</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection