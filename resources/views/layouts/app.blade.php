@extends('layouts.base')

@section('body')
    <div class="container-fluid py-4">
        @yield('content')
    </div>
@endsection