@extends('layouts.base')

@section('body')
    <div class="container-fluid py-4">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="row justify-content-center">
                    <div class="w-50 text-center">       
                        @include('alert')
                    </div>        
                </div>
                @yield('content')
            </div>
        </div>
    </div>
@endsection