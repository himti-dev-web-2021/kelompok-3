<?php

use App\Http\Controllers\{
    ArtistController,
    ExploreController,
    GenreController,
    HomeController,
    MusicController,
    PermintaanController,
};
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', [ExploreController::class, 'index'])->name('welcome');

Auth::routes();

Route::get('/profile', [HomeController::class, 'index'])->name('home');

Route::middleware('auth')->group(function() {
    Route::prefix('admin-menu')->group(function() {
        Route::prefix('genre')->group(function() {
            Route::get('', [GenreController::class, 'index'])->name('genre');
            Route::get('tambah', [GenreController::class, 'create'])->name('genre.tambah');
            Route::post('save', [GenreController::class, 'store'])->name('genre.save');
            Route::get('edit/{uuid}', [GenreController::class, 'edit'])->name('genre.edit');
            Route::put('update/{uuid}', [GenreController::class, 'update'])->name('genre.update');
            Route::delete('delete/{genre:id}', [GenreController::class, 'destroy'])->name('genre.delete');
        });
        
        Route::prefix('artist')->group(function() {
            Route::get('', [ArtistController::class, 'index'])->name('artist');
            Route::get('tambah', [ArtistController::class, 'create'])->name('artist.tambah');
            Route::post('save', [ArtistController::class, 'store'])->name('artist.save');
            Route::get('edit/{uuid}', [ArtistController::class, 'edit'])->name('artist.edit');
            Route::put('update/{uuid}', [ArtistController::class, 'update'])->name('artist.update');
            Route::delete('delete/{artist:id}', [ArtistController::class, 'destroy'])->name('artist.delete');
        });
        
        Route::prefix('music')->group(function() {
            Route::get('', [MusicController::class, 'index'])->name('music');
            Route::get('tambah', [MusicController::class, 'create'])->name('music.tambah');
            Route::post('save', [MusicController::class, 'store'])->name('music.save');
            Route::get('edit/{uuid}', [MusicController::class, 'edit'])->name('music.edit');
            Route::put('update/{uuid}', [MusicController::class, 'update'])->name('music.update');
            Route::delete('delete/{music:id}', [MusicController::class, 'destroy'])->name('music.delete');
        });
    });
    
    Route::prefix('user-menu')->group(function() {
        Route::prefix('permintaan')->group(function() {
            Route::get('', [PermintaanController::class, 'index'])->name('permintaan');
            Route::get('tambah', [PermintaanController::class, 'create'])->name('permintaan.tambah');
            Route::post('save', [PermintaanController::class, 'store'])->name('permintaan.save');
            Route::get('edit/{uuid}', [PermintaanController::class, 'edit'])->name('permintaan.edit');
            Route::put('update/{uuid}', [PermintaanController::class, 'update'])->name('permintaan.update');
            Route::delete('delete/{permintaan:id}', [PermintaanController::class, 'destroy'])->name('permintaan.delete');
        });
    });
});

Route::prefix('explore')->group(function() {
    Route::get('artist', [ExploreController::class, 'indexName'])->name('explore');
    Route::get('artist/sort-by-genre', [ExploreController::class, 'indexGenre'])->name('explore.bygenre');
    Route::get('artist/{artist:name}', [ExploreController::class, 'showArtist'])->name('explore.showartist');
    Route::get('music/{uuid}', [ExploreController::class, 'showMusic'])->name('explore.showmusic');
    Route::get('genre/{genre:name}', [ExploreController::class, 'showGenre'])->name('explore.showgenre');
    Route::get('search', [ExploreController::class , 'searchMusic'])->name('explore.searchmusic');
});