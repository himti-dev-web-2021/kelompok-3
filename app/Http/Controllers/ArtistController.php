<?php

namespace App\Http\Controllers;

use App\Helpers\UploadHelper;
use App\Http\Requests\ArtistRequest;
use App\Models\{Artist, Genre, User};
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Ramsey\Uuid\Uuid;

class ArtistController extends Controller
{
    public function index(User $user)
    {
        $this->authorize('index', $user);

        return view('artist.index', [
            'artist' => Artist::orderBy('name', 'asc')->paginate(10),
        ]);        
    }

    public function create(User $user)
    {
        $this->authorize('create', $user);

        return view('artist.tambah', [
            'genre' => Genre::orderBy('name', 'asc')->get(),
        ]);
    }

    public function store(ArtistRequest $request)
    {
        $attr = $request->validated();
        $attr['uuid'] = Uuid::uuid4()->toString();
        if (!empty($request->file('foto'))) {
            $attr['foto'] = (new UploadHelper)->prosesUpload($request->file('foto') ? request()->file('foto') : null);
        }
        Artist::create($attr);
        return redirect()->route('artist')->with('artist', 'Artist was created');
    }

    public function edit(Artist $artist, User $user, string $uuid)
    {
        $this->authorize('edit', $user);

        return view('artist.edit', [
            'artist' => $artist::where('uuid', $uuid)->first(),
            'genre' => Genre::orderBy('name', 'asc')->get(),
        ]);
    }

    public function update(ArtistRequest $request, Artist $artist, string $uuid)
    {
        $attr = $request->validated();
        if (!empty($request->file('foto'))) {
            $attr['foto'] = (new UploadHelper)->prosesUpload($request->file('foto') ? request()->file('foto') : null);
        } else {
            $attr['foto'] = $artist->foto;
        }
        $artist->where('uuid', $uuid)->update($attr);
        return redirect()->route('artist')->with('artist', 'Artist was updated');
    }

    public function destroy(Artist $artist)
    {
        $artist->delete();
        return redirect()->route('artist')->with('artist', 'Artist was deleted');
    }
}
