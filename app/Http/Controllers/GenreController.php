<?php

namespace App\Http\Controllers;

use App\Http\Requests\GenreRequest;
use App\Models\Genre;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Ramsey\Uuid\Uuid;

class GenreController extends Controller
{
    public function index(User $user)
    {
        $this->authorize('index', $user);
        return view('genre.index', [
            'genre' => Genre::orderBy('name', 'asc')->paginate(10),
        ]);        
    }

    public function create(User $user)
    {        
        $this->authorize('create', $user);
        return view('genre.tambah');
    }

    public function store(GenreRequest $request)
    {
        $attr = $request->validated();
        $attr['uuid'] = Uuid::uuid4()->toString();
        Genre::create($attr);
        return redirect()->route('genre')->with('genre', 'Genre was created');
    }

    public function edit(Genre $genre, User $user, string $uuid)
    {
        $this->authorize('edit', $user);
        return view('genre.edit', [
            'genre' => $genre::where('uuid', $uuid)->first()
        ]);
    }

    public function update(GenreRequest $request, Genre $genre, string $uuid)
    {
        $attr = $request->validated();
        $genre->where('uuid', $uuid)->update($attr);
        return redirect()->route('genre')->with('genre', 'Genre was updated');
    }

    public function destroy(Genre $genre)
    {
        $genre->delete();
        return redirect()->route('genre')->with('genre', 'Genre was deleted');
    }
}
