<?php

namespace App\Http\Controllers;

use App\Http\Requests\PermintaanRequest;
use App\Models\Permintaan;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Ramsey\Uuid\Uuid;

class PermintaanController extends Controller
{
    public function index(User $user)
    {
        $this->authorize('index', $user);

        return view('permintaan.index', [
            'permintaan' => Permintaan::orderBy('artist', 'asc')->paginate(10),
        ]);        
    }

    public function create()
    {
        return view('permintaan.tambah');
    }

    public function store(PermintaanRequest $request)
    {
        $attr = $request->validated();
        $attr['uuid'] = Uuid::uuid4()->toString();
        $attr['user_id'] = Auth::user()->id;
        Permintaan::create($attr);
        return redirect()->route('home')->with('permintaan', 'Request was created');
    }

    public function edit(Permintaan $permintaan, string $uuid)
    {
        return view('permintaan.edit', [
            'permintaan' => $permintaan::where('uuid', $uuid)->first()
        ]);
    }

    public function update(PermintaanRequest $request, Permintaan $permintaan, string $uuid)
    {
        $attr = $request->validated();
        $attr['user_id'] = Auth::user()->id;
        $permintaan->where('uuid', $uuid)->update($attr);
        return redirect()->route('home')->with('permintaan', 'Request was updated');
    }

    public function destroy(Permintaan $permintaan)
    {
        $permintaan->delete();
        return redirect()->route('home')->with('permintaan', 'Request was deleted');
    }
}
