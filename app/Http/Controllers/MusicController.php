<?php

namespace App\Http\Controllers;

use App\Helpers\UploadHelper;
use App\Http\Requests\MusicRequest;
use App\Models\{Artist, Music, User};
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Ramsey\Uuid\Uuid;

class MusicController extends Controller
{
    public function index(User $user)
    {
        $this->authorize('index', $user);

        return view('music.index', [
            'music' => Music::orderBy('artist_id', 'asc')->paginate(10),
        ]);        
    }

    public function create(User $user)
    {
        $this->authorize('create', $user);

        return view('music.tambah', [
            'artist' => Artist::get(),
        ]);
    }

    public function store(MusicRequest $request)
    {
        $attr = $request->validated();
        $attr['uuid'] = Uuid::uuid4()->toString();
        Music::create($attr);
        return redirect()->route('music')->with('music', 'Music was created');
    }

    public function edit(Music $music, User $user, string $uuid)
    {
        $this->authorize('edit', $user);

        return view('music.edit', [
            'music' => $music::where('uuid', $uuid)->first(),
            'artist' => Artist::get(),
        ]);
    }

    public function update(MusicRequest $request, Music $music, string $uuid)
    {
        $attr = $request->validated();
        
        $music->where('uuid', $uuid)->update($attr);
        return redirect()->route('music')->with('music', 'Music was updated');
    }

    public function destroy(Music $music)
    {
        $music->delete();
        return redirect()->route('music')->with('music', 'Music was deleted');
    }
}
