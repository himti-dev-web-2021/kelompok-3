<?php

namespace App\Http\Controllers;

use App\Models\Artist;
use App\Models\Genre;
use App\Models\Music;
use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;

class ExploreController extends Controller
{
    public function index()
    {
        return view('welcome', [
            'genre' => Genre::orderBy('name', 'asc')->get(),
            'music' => Music::latest()->limit(20)->get(),
        ]);
    }

    public function indexName()
    {
        return view('explore.index', [
            'artist' => Artist::orderBy('name', 'asc')->paginate(8),
        ]);  
    }
    
    public function indexGenre()
    {
        return view('explore.index_genre', [
            'artist' => Artist::orderBy('genre_id', 'asc')->paginate(8),
        ]);  
    }

    public function showArtist(Artist $artist)
    {
        $artists = Artist::where('name', $artist->name)->first();
        return view('explore.show_artist', [
            'artist' => $artists,
            'music' => Music::where('artist_id', $artist->id)->orderBy('tgl', 'asc')->paginate(8),
        ]);
    }

    public function showMusic(Music $music, string $uuid)
    {
        $music = $music::where('uuid', $uuid)->first();
        return view('explore.show_music', [
            'music' => $music,
        ]);
    }

    public function showGenre(Genre $genre)
    {
        $genre = Genre::where('name', $genre->name)->first();
        return view('explore.show_genre', [
            'genre' => $genre,
            'artist' => Artist::where('genre_id', $genre->id)->orderBy('name', 'asc')->paginate(8),
        ]);
    }

    public function searchMusic()
    {
        $query = request('query');

        $music = Music::where("title", "like", "%$query%")->latest()->paginate(8);
        return view('explore.search_music', [
            'music' => $music,
            'query' => $query
        ]);
    }
}
