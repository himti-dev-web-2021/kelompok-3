<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ArtistRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'genre_id' => ['required'],
            'name' => ['required', 'string'],
            'bio' => ['required', 'string'],
            'foto' => ['nullable', 'mimes:jpeg,jpg,png'],
        ];
    }
}
