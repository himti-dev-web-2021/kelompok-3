<?php
namespace App\Helpers;

class UploadHelper
{
    public function prosesUpload($req_file)
    {
        $file = $req_file;
        $nama_file = time() . "_" . $file->getClientOriginalName();
        $tujuan_upload = 'data_file';
        $file->move($tujuan_upload, $nama_file);
        return $nama_file;
    }
}
