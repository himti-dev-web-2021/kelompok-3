<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Music extends Model
{
    use HasFactory;
    
    protected $table = 'music';
    protected $fillable = ['uuid', 'artist_id', 'title', 'tgl', 'url', 'lyric', 'writer'];

    public function artist() {
        return $this->belongsTo(Artist::class);
    }
}
