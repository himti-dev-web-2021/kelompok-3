<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ArtistPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(User $user) {
        return $user->username === 'admin';
    }

    public function create(User $user) {
        return $user->username === 'admin';
    }

    public function edit(User $user) {
        return $user->username === 'admin';
    }
}
